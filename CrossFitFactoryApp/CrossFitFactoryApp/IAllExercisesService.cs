﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace CrossFitFactoryApp
{
    public interface IAllExercisesService
    {
        event Action<ObservableCollection<ExerciseModel>> OnSetExercisesList;

        Task SetExercisesList();
    }
}