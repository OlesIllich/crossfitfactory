﻿namespace CrossFitFactoryApp
{
    public partial class AllExerciseView
    {
        public AllExerciseView(IAllExercisesService service)
        {
            InitializeComponent();
            BindingContext = new AllExercisesViewModel(service).Initialize();
        }
    }
}