﻿using System;
using System.Collections.ObjectModel;
using System.Windows.Input;
using Xamarin.Forms;

namespace CrossFitFactoryApp
{
    public class AllExercisesViewModel : ViewModelBase
    {
        private ObservableCollection<ExerciseModel> _exercises = new ObservableCollection<ExerciseModel>();
        private ExerciseModel _selectedItem;

        private readonly IAllExercisesService _service;

        public ICommand ImageClickedCommand { get; set; }
        public string AllExercisesName => "CrossFit Exercises";

        public ObservableCollection<ExerciseModel> Exercises
        {
            set => SetProperty(ref _exercises, value);
            get => _exercises;
        }

        public ExerciseModel SelectedItem
        {
            set => SetProperty(ref _selectedItem, value);
            get => _selectedItem;
        }

        public AllExercisesViewModel(IAllExercisesService service)
        {
            _service = service;
            _service.OnSetExercisesList += e => Exercises = e;
        }

        public AllExercisesViewModel Initialize()
        {
            ImageClickedCommand = new Command(() => Device.OpenUri(new Uri(SelectedItem.VideoUrl)));
            return this;
        }
    }
}