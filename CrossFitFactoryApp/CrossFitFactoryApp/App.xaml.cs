﻿namespace CrossFitFactoryApp
{
    public partial class App
    {
        private IAllExercisesService _service;

        public App()
        {
            InitializeComponent();

            _service = new AllExercisesService();
            MainPage = new AllExerciseView(_service);
        }

        protected  override async void OnStart()
        {
            await _service.SetExercisesList();
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}