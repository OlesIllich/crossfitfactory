﻿namespace CrossFitFactoryApp
{
    public class ExerciseModel
    {
        public string Name { get; set; }

        public string VideoUrl { get; set; }

        public string Icon { get; set; }
    }
}