﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace CrossFitFactoryApp
{
    public class AllExercisesService : IAllExercisesService
    {
        public event Action<ObservableCollection<ExerciseModel>> OnSetExercisesList;

        public async Task SetExercisesList()
        {
            OnSetExercisesList?.Invoke(await GetExercisesList());
        }

        private Task<ObservableCollection<ExerciseModel>> GetExercisesList()
        {
            //TODO download list from file or link
            return Task.FromResult(new ObservableCollection<ExerciseModel>
            {
                new ExerciseModel
                {
                    Name = "Deadlift",
                    VideoUrl = "https://www.youtube.com/watch?time_continue=1&v=op9kVnSso6Q",
                    Icon = "https://i.ytimg.com/vi/op9kVnSso6Q/maxresdefault.jpg"
                },
                new ExerciseModel()
                {
                    Name = "Front Squat",
                    VideoUrl = "https://www.youtube.com/watch?time_continue=1&v=m4ytaCJZpl0",
                    Icon = "https://i.ytimg.com/vi/m4ytaCJZpl0/maxresdefault.jpg"
                },
                new ExerciseModel()
                {
                    Name = "Overhead Squat",
                    VideoUrl = "https://www.youtube.com/watch?v=RD_vUnqwqqI",
                    Icon = "https://i.ytimg.com/vi/RD_vUnqwqqI/maxresdefault.jpg"
                },
                new ExerciseModel()
                {
                    Name = "The Air Squat",
                    VideoUrl = "https://www.youtube.com/watch?v=C_VtOYc6j5c&list=PLBrgqsGCyn2xQVBgDaemdoK8TJlgSpX0G",
                    Icon = "https://www.crossfit.com/assets/img/sites/exercisedemos/airsquat.jpg"
                },
                new ExerciseModel()
                {
                    Name = "Shoulder Press",
                    VideoUrl = "https://www.youtube.com/watch?time_continue=1&v=xe19t2_6yis",
                    Icon = "https://www.crossfit.com/assets/img/sites/exercisedemos/shoulderpress.jpg"
                },
                new ExerciseModel()
                {
                    Name = "Push Press",
                    VideoUrl = "https://www.youtube.com/watch?time_continue=2&v=X6-DMh-t4nQ",
                    Icon = "https://www.crossfit.com/assets/img/sites/exercisedemos/pushpress.jpg"
                },
                new ExerciseModel()
                {
                    Name = "The Wall Ball",
                    VideoUrl =
                        "https://www.youtube.com/watch?v=fpUD0mcFp_0&index=5&list=PLBrgqsGCyn2xQVBgDaemdoK8TJlgSpX0G",
                    Icon = "https://www.crossfit.com/assets/img/sites/exercisedemos/wallball.jpg"
                }
            });
        }
    }
}