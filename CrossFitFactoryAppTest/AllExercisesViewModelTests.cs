﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using AutoFixture.Xunit2;
using CrossFitFactoryApp;
using FluentAssertions;
using NSubstitute;
using Xunit;

namespace CrossFitFactoryAppTest
{
    public class AllExercisesViewModelTests
    {
        [Theory]
        [AutoData]
        public void ChangeExercisesListWhenServiceGetNewList(ObservableCollection<ExerciseModel> modelList)
        {
            //Arrange
            var service = Substitute.For<IAllExercisesService>();
            var viewModel= new AllExercisesViewModel(service);

            //Act
            service.OnSetExercisesList += Raise.Event<Action<ObservableCollection<ExerciseModel>>>(modelList);

            //Assert
            viewModel.Exercises.Should().BeEquivalentTo(modelList);
        }


    }
}
